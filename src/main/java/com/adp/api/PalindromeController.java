package com.adp.api;

import com.adp.model.Palindrome;
import com.adp.service.PalindromeService;
import com.adp.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = Constants.APIURI_PREFIX)
public class PalindromeController {
    @Autowired
    private PalindromeService palindromeService;
    @RequestMapping(value = "/palindrome/{num}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Palindrome> getPalindrome(final @PathVariable(value = "num") Integer num) {

        return ResponseEntity.ok(palindromeService.closestPalindrome(num));
    }
}
