package com.adp.model;

import java.io.Serializable;

public class Palindrome implements Serializable {
    private Integer result;


    public Integer getResult() {
        return result;
    }

    public void setResult(Integer result) {
        this.result = result;
    }

    public Palindrome(Integer result) {
        this.result = result;
    }
}
