package com.adp.service.impl;

import com.adp.model.Palindrome;
import com.adp.service.PalindromeService;

@org.springframework.stereotype.Service
public class PalindromeServiceImpl implements PalindromeService {

    /**
     * @param polindrome Integer number for which closes polindrome to be found
     * @return Palindrome object where result filed contains closest polindrome
     */
    public Palindrome closestPalindrome(Integer polindrome) {
        String n = polindrome.toString();
        int order = (int) Math.pow(10, n.length() / 2);
        Long ans = Long.valueOf(new String(n));
        Long noChange = reverse(ans);
        Long larger = reverse((ans / order) * order + order + 1);
        Long smaller = reverse((ans / order) * order - 1);
        if (noChange > ans) {
            larger = (long) Math.min(noChange, larger);
        } else if (noChange < ans) {
            smaller = (long) Math.max(noChange, smaller);
        }
        String s = String.valueOf(ans - smaller <= larger - ans ? smaller : larger);
        Palindrome pol = new Palindrome(Integer.valueOf(s));
        return pol;
    }

    /**
     * @param num Number to reverse
     * @return reversed mirrored number
     */
    private Long reverse(Long num) {
        char[] a = String.valueOf(num).toCharArray();
        int i = 0;
        int j = a.length - 1;
        while (i < j) {
            a[j--] = a[i++];
        }
        return Long.valueOf(new String(a));
    }
}
