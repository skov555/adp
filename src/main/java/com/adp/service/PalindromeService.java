package com.adp.service;

import com.adp.model.Palindrome;

public interface PalindromeService {

     Palindrome closestPalindrome(Integer polindrome);
}
