package com.adp;

import com.adp.api.PalindromeController;
import com.atlassian.oai.validator.springmvc.InvalidRequestException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc(addFilters = true)
public class OpenApiValidatorTests {
    @Autowired
    MockMvc mockMvc;

    @Test
    public void testGetValidPalindrome() throws Exception {
        final PalindromeController palindromeController = new PalindromeController();
        mockMvc.perform(get("/api/v1/palindrome/123")).andExpect(status().isOk()).andExpect(jsonPath("$.result", 1)
                .value(equalTo(121)));

    }

    @Test
    public void testGetInvalidRequest() throws Exception {
        ResultActions perform = mockMvc.perform(get("/api/v1/palindrome/test"));
        assert perform.andReturn().getResolvedException() instanceof InvalidRequestException;

    }

    /**
     * Test a GET with an invalid request/response expectation.
     * <p>
     * This test will pass the business logic tests, but the validation filter will fail the test because even though
     * the server returned a valid result the request used a path parameter that does not match the schema defined
     * in the API specification.
     *
     * This test is expected to FAIL
     */
//    @Test
//    public void testGetWithInvalidId() throws Exception {
//        final PetController petController = new PetController();
//        final MockMvc mockMvc = MockMvcBuilders.standaloneSetup(petController).build();
//
//        mockMvc
//                .perform(get("/pet/ONE")
//                        .header("api_key", "API_KEY"))
//                .andExpect(status().isOk())
//                .andExpect(openApi().isValid(SWAGGER_JSON_URL))
//                .andExpect(jsonPath("$.id", 1)
//                        .value(equalTo(1)));
//    }
}
